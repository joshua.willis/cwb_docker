FROM registry.gitlab.com/gwburst/public/cwb_docker:compilation_v1.2

# Setting envs 
ARG username=cWB_docker
ENV HOME /home/${username}
ENV HOME_LIBS ${HOME}/SOFT
ENV CWB_DOCKER_VERSION cwb-6.4.2
WORKDIR /home/${username}
ARG root_version=v6-24-06
ENV ROOT_VERSION=root-${root_version}
# Switch to our newly created user
USER ${username}
SHELL ["/bin/zsh", "-c"]
#Python Setup

RUN export PYTHONPATH="" \
&& python3 -m venv ~/virtualenv/pesummary \
&& source ~/virtualenv/pesummary/bin/activate \
&& pip3 install --upgrade pip \
&& pip3 install --trusted-host pypi.python.org -r requirements.txt


#Uncommenting lines
RUN cd /home/${username} \
&& sed '/Rint.Logon:/s/^#//g' -i .rootrc \
&& sed '/source/s/^#//g' -i docker_watenv.sh \
&& sed -e '/cg\//s/^#//g' -e '/pesummary/s/^#//g' -i .zshrc 
#&& sed -i '/^[^#]/ s/\(^.*thisroot.*$\)/#\ \1/' .zshrc

#Cloning public config
RUN cd /home/${username}/git/cWB/ \
&& git clone -b 'public' --single-branch --depth 1 https://gitlab.com/gwburst/public/config_o3.git  config\
&& cd config \
#&& git lfs pull -I XTALKS/wdmXTalk/OverlapCatalog16-1024.bin \
#&& git lfs pull -I XTALKS/wdmXTalk/OverlapCatalog-ilLev3-hLev9-iNu6-P10.xbin \
&& git lfs pull -I "XTALKS/wdmXTalk/*" \ 
&& ln -s /home/${username}/git/cWB/config/XTALKS/wdmXTalk/* /home/${username}/SOFT/WAT/filters/wdmXTalk/. 

RUN  cd /home/${username}/git/cWB/config \
&& make DATA=GWOSC O3=O3b 
#&& cd GWOSC \
#&& make \ 
#&& cd ../.. \
#&& cp /home/${username}/git/cWB/config/XTALKS/wdmXTalk/OverlapCatalog16-1024.bin /home/${username}/SOFT/WAT/filters/wdmXTalk/. 

#Cloning and building of public library
RUN cd /home/${username}/git/cWB/ \
&& git clone -b 'public' --single-branch https://gitlab.com/gwburst/public/library.git \
#&& git clone -b 'shared_franz' --single-branch https://gitlab+deploy-token-1:XXXXXXXX@gitlab.com/gwburst/shared/library.git \
&& cd library \
&& mv /home/${username}/docker_watenv.sh /home/${username}/git/cWB/library/docker_watenv.sh \
&& source docker_watenv.sh \
&& make deb_syspkg \ 
&& zsh -ie build.sh

#Configurations 
RUN cd /home/${username}/git/cWB/library \
&& source docker_watenv.sh \
&& source tools/install/etc/cwb/cwb-activate.sh \
&& cd tools/cwb/www \
&& make \
&& cd ../../.. \
&& tools/cwb/scripts/cwb_create_www.sh \
&& cd ../../..

#&& make winstall ODIR=/home/${username}/SOFT/cWB/library/public_install \
#&& mv /home/${username}/git/cWB/library/docker_watenv.sh home/${username}/SOFT/cWB/library/public_install/docker_watenv.sh \
#&& rm -rf /home/${username}/git/lalsuite 
#&& source /home/${username}/SOFT/cWB/library/public_install/docker_watenv.sh

#USER root

#When the container is started with
# docker run -it registry.gitlab.com/gwburst/public/cwb_docker:latest
# the default is to start a zsh loging shell as the cWB_docker user. 
CMD ["/bin/zsh"]
