# cWB pipeline shared docker image

This repository contains all source files to
to build a cWB pipeline docker image 

## Cloning the cWB shared docker image repository

    git clone git@gitlab.com:gwburst/public/cwb_docker.git

## Current branches

branch  | purpose
------------- | -------------
public    | all-purpose image based on debian buster

## How to build and run the docker image

0. Download all needed precompiled packages (i.e. CERN root, lal, etc.)

```
     git lfs pull -I "archives/*.bz2"   
```

1. Login and pull the starting image, e.g. debian buster

```
     docker pull debian:buster
```

2. from within the cwb_docker git repository directory launch the building process: depending on the ammount of compilation/installation, it might take from a couple of minutes to hours (if you recompile everything).

```
    docker build -t cwb_docker:test
```

3. Once the image has been built locally, you can run it with :

    - LINUX: 

    ```
        docker run -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -it cwb_docker:test
    ```

    - WINDOWS: 

    ```
        docker run --interactive --tty cwb_docker:test
    ```
## How to use the gitlab Container Registry

First log in to GitLab’s Container Registry using your GitLab username and password. If you have 2FA enabled you need to use a personal access token:

    docker login registry.gitlab.com


You can also use a deploy token for read-only access to the registry images.

Once you log in, you’re free to create, push or pull a container image using the common build, push and pull commands

    docker build -t registry.gitlab.com/gwburst/public/cwb_docker:buster_vX.Y .
    docker push registry.gitlab.com/gwburst/public/cwb_docker:buster_vX.Y
    docker pull registry.gitlab.com/gwburst/public/cwb_docker:buster_vX.Y
